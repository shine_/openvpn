Name: openvpn
Version: 2.4.8
Release: 6
Summary: A full-featured open source SSL VPN solution
License: GPLv2 and OpenSSL and SSLeay
URL: https://community.openvpn.net/openvpn
Source0: https://swupdate.openvpn.org/community/releases/openvpn-%{version}.tar.gz
Patch0000:     CVE-2020-11810.patch
Patch0001:     CVE-2020-15078.patch
BuildRequires: openssl-devel lz4-devel systemd-devel lzo-devel gcc
BuildRequires: iproute pam-devel pkcs11-helper-devel >= 1.11

Requires: iproute
Requires(pre): /usr/sbin/useradd

%description
OpenVPN is a full-featured open source SSL VPN solution that accommodates a wide range of configurations, 
including remote access, site-to-site VPNs, Wi-Fi security, and enterprise-scale remote access solutions with load balancing, 
failover, and fine-grained access-controls. Starting with the fundamental premise that complexity is the enemy of security, 
OpenVPN offers a cost-effective, lightweight alternative to other VPN technologies that is well-adapted for the SME and enterprise markets.

%package devel
Summary: Development headers and examples for OpenVPN plug-ins

%description devel
OpenVPN can be extended through the --plugin option, which provides possibilities to add specialized authentication, 
user accounting, packet filtering and related features. These plug-ins need to be written in C and 
provides a more low-level and information rich access to similar features as the various script-hooks.

%package help
Summary: Documents for %{name}
BuildArch: noarch

%description help
User guide and other related documents for %{name}.


%prep
%autosetup -n %{name}-%{version} -p1

%build
%configure --enable-x509-alt-username --enable-iproute2 --with-crypto-library=openssl --enable-pkcs11 --enable-selinux --enable-systemd SYSTEMD_UNIT_DIR=%{_unitdir} TMPFILES_DIR=%{_tmpfilesdir} IPROUTE=/sbin/ip
%make_build


%install
%make_install
%delete_la
mkdir -p -m 0750 $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/client $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/server
cp sample/sample-config-files/client.conf $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/client
cp sample/sample-config-files/server.conf $RPM_BUILD_ROOT%{_sysconfdir}/%{name}/server

mkdir -m 0750 -p $RPM_BUILD_ROOT%{_rundir}/%{name}-{client,server}
mkdir -m 0770 -p $RPM_BUILD_ROOT%{_sharedstatedir}/%{name}

cp -a contrib sample $RPM_BUILD_ROOT%{_pkgdocdir}

%check
make check


%pre
getent group openvpn &>/dev/null || groupadd -r openvpn
getent passwd openvpn &>/dev/null || \
    /usr/sbin/useradd -r -g openvpn -s /sbin/nologin -c OpenVPN \
        -d /etc/openvpn openvpn

%post
if [ $1 -eq 1 ] ; then
        # Initial installation
        systemctl --no-reload preset openvpn-client@\*.service &>/dev/null || :
fi


if [ $1 -eq 1 ] ; then
        # Initial installation
        systemctl --no-reload preset openvpn-server@\*.service &>/dev/null || :
fi

%preun
if [ $1 -eq 0 ] ; then 
        # Package removal, not upgrade 
        systemctl --no-reload disable --now openvpn-client@\*.service &>/dev/null || : 
fi 


if [ $1 -eq 0 ] ; then 
        # Package removal, not upgrade 
        systemctl --no-reload disable --now openvpn-server@\*.service &>/dev/null || : 
fi

%postun
if [ $1 -ge 1 ] ; then 
        # Package upgrade, not uninstall 
        systemctl try-restart openvpn-client@\*.service &>/dev/null || : 
fi 


if [ $1 -ge 1 ] ; then 
        # Package upgrade, not uninstall 
        systemctl try-restart openvpn-server@\*.service &>/dev/null || : 
fi 

%files
%license AUTHORS COPYING COPYRIGHT.GPL
%config %{_sysconfdir}/%{name}/*/*
%{_sbindir}/%{name}
%{_libdir}/%{name}/
%{_unitdir}/%{name}-client@.service
%{_unitdir}/%{name}-server@.service
%{_tmpfilesdir}/%{name}.conf
%attr(0750,-,-) %{_rundir}/%{name}-client
%attr(0750,-,-) %{_rundir}/%{name}-server
%attr(0770,openvpn,openvpn) %{_sharedstatedir}/%{name}

%files devel
%{_pkgdocdir}/sample/sample-plugins
%{_includedir}/openvpn-plugin.h
%{_includedir}/openvpn-msg.h

%files help
%{_pkgdocdir}
%{_mandir}/man8/%{name}.8*

%changelog
* Wed Jun 9 2021 zhaoyao <zhaoyao32@huawei.com> - 2.4.8-6
- fix faileds: /bin/sh: gcc: command not found.

* Tue May 25 2021 wangyue <wangyue92@huawei.com> - 2.4.8-5
- fix CVE-2020-15078

* Thu Feb 04 2021 wangyue <wangyue92@huawei.com> 2.4.8-4
- fix CVE-2020-11810

* Tue Mar 16 2020 daiqianwen <daiqianwen@huawei.com> 2.4.8-3
- modify systemd post preun postun

* Mon Nov 11 2019 guanyalong <guanyalong@huawei.com> 2.4.8-2
- Type:enhancement
- ID:NA
- SUG:NA
- DESC:enable pkcs11 support

* Thu Nov 7 2019 openEuler Buildteam <buildteam@openeuler.org> - 2.4.8.1
- Package init
